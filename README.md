## redfin-user 11 RQ3A.210605.005 7349499 release-keys
- Manufacturer: xiaomi
- Platform: sdm660
- Codename: lavender
- Brand: Xiaomi
- Flavor: fuse_lavender-userdebug
- Release Version: 12
- Id: SD1A.210817.036.A8
- Incremental: eng.selste.20211201.105805
- Tags: test-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:11/RQ3A.210605.005/7349499:user/release-keys
- OTA version: 
- Branch: redfin-user-11-RQ3A.210605.005-7349499-release-keys
- Repo: xiaomi_lavender_dump


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
